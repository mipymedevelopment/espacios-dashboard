import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-upload-work',
  templateUrl: './upload-work.component.html',
  styleUrls: ['./upload-work.component.scss']
})
export class UploadWorkComponent implements OnInit {

  constructor(private api:ApiService, private router:Router) { }

  image:any = null;
  titulo:string = ""
  desc:string = ""

  message:string | null = null;
  disabledButton:boolean = false;

  ngOnInit(): void {
  }

  handleChange(event:any){
    this.message = null
    const reader = new FileReader()
    reader.onloadend = () =>{
      this.image = reader.result
    }
    reader.readAsDataURL(event.target.files[0])
  }

  uploadWork(){

    if(this.disabledButton){
      return;
    }

    if(!this.image){
      this.message = "No file has been selected"
      return;
    }

    if(!this.titulo.length){
      this.message = "You must provide a title"
      return;
    }

    if(!this.desc.length){
      this.message = "You must provide a description for your work"
      return;
    }

    this.disabledButton = true;

    this.api.uploadWork(this.titulo,this.image,this.desc).subscribe(response =>{
      let res:any = response
      if(res.status){
        this.message = "Your work has been uploaded successfully"
        setTimeout(() => {
          this.router.navigate(['dashboard'])        
        }, 3000);
      }
      else{
        this.disabledButton = false;
        this.message = "Server error"
      }
    })
        
  }
  // avisar que todo salio bn
}

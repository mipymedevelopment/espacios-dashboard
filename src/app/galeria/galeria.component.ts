import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.scss']
})
export class GaleriaComponent implements OnInit {

  constructor(private api: ApiService) { }

  works: any = []
  indexed_works: any = []
  index: number = 0

  openMenu: boolean = false;
  message:string | null = null;

  ngOnInit(): void {
    this.api.getWorks().subscribe(response => {
      this.works = response
      for (let i = 0; i < this.works.length; i++) {
        if (this.works[i].index != null) {
          this.indexed_works.push(this.works[i])
        }
      }
      this.indexed_works.sort( (work1:any, work2:any) => (work1.index > work2.index) )      
    })
  }

  iplusplus() {
    this.index = (this.index + 1) % this.indexed_works.length
  }

  imenosmenos() {
    this.index -= 1
    if (this.index < 0) {
      this.index = this.indexed_works.length - 1
    }
  }

  toggleMenu() {
    this.openMenu = !this.openMenu
  }

  selectWork(id: string) {

    let new_work = this.works.find((work:any) =>(work._id == id))
    let new_work_index = this.indexed_works.findIndex( (work:any) => (new_work._id == work._id) )

    if(new_work_index == -1){
      this.indexed_works[this.index] = new_work 
    }
    else{
      this.indexed_works.splice(new_work_index, 1)
      if(new_work_index < this.index){
        this.index -= 1
      }
      this.indexed_works[this.index] = new_work
    }

    // buscar imagen en la lista de indexados
    // si no esta indexada simplmente reemplazar
    // si si esta indexada, sacarla
    //    si el indice es mayor que el actual:  nada
    //    si el indice es menor que el actual indice--
    // poner obra

    this.toggleMenu()
  }

  updateIndex(){
    
    for(let i=0 ; i<this.works.length ; i++){
      this.works[i].index = null
    }

    for(let i=0 ; i<this.indexed_works.length ; i++){
      this.indexed_works[i].index = i
    }


    let works_index = []                              // guardar datos
    for(let i=0 ; i<this.works.length ; i++){
      works_index.push({
        _id: this.works[i]._id,
        index: this.works[i].index
      })
    }
    this.api.updateLinks(works_index).subscribe(response =>{    // enviar al servidor
      let res:any = response
      if(res.recived){
        this.message = "Changes updated"
        setTimeout(() => {
          this.message = null
        }, 3000);
      }
    }) 
  }

  addNewSlot(){
    let randomWork = this.works.find( (work:any) => (work.index == null) )
    this.indexed_works.push(randomWork)
    this.index += 1
  }

  removeSlot(){
    this.indexed_works.splice(this.index, 1)
  }
}

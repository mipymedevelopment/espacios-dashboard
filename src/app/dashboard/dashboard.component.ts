import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private api:ApiService) { }

  info:any = false;
  showProfileImageControls = false;   // profile image
  newProfileImage:any;

  newName:string = ""
  showNameControls = false;
  newDesc:string = ""
  showDescControls:boolean = false;

  message:string | null = null

  ngOnInit(): void {
    this.getInfo()
  }

  getInfo(){
    this.api.getInfo().subscribe(response =>{
      this.info = response;
      this.newName = this.info.name
      this.newDesc = this.info.desc
    })
  }

  handleChange(file:any){
    this.newProfileImage = file.target.files[0]
  }

  uploadProfileImage(){
    const reader = new FileReader()
    reader.onloadend = () =>{
      this.api.changeProfileImage(reader.result).subscribe(response =>{
        let res:any = response
        if(res.status){
          this.getInfo()
        }
      })
    }
    reader.readAsDataURL(this.newProfileImage)
  }

  swichProfileImageControls(){
    this.showProfileImageControls = !this.showProfileImageControls
  }

  switchNameControls(){
    this.showNameControls = !this.showNameControls
  }

  updateName(){
    this.api.updateInfo(this.newName, this.info.desc).subscribe(response =>{
      let res:any = response
      if(res.status){
        this.switchNameControls()
        this.info.name = this.newName
        this.message = "Profile updated!"
        setTimeout(() => {
          this.message = null
        }, 3000);
      }
    })
  }

  switchDescControls(){
    this.showDescControls = !this.showDescControls
  }

  updateDesc(){
    this.api.updateInfo(this.info.name, this.newDesc).subscribe(response =>{
      let res:any = response
      if(res.status){
        this.switchDescControls()
        this.info.desc = this.newDesc
        this.message = "Profile updated!"
        setTimeout(() => {
          this.message = null
        }, 3000);
      }
    })
  }

}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MyinterceptorInterceptor } from './utils/myinterceptor.interceptor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UploadWorkComponent } from './upload-work/upload-work.component';
import { NavbarComponent } from './navbar/navbar.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { EditWorkComponent } from './edit-work/edit-work.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    UploadWorkComponent,
    NavbarComponent,
    GaleriaComponent,
    EditWorkComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: MyinterceptorInterceptor,
    multi: true
}],
  bootstrap: [AppComponent]
})
export class AppModule { }

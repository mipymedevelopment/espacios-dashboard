import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-edit-work',
  templateUrl: './edit-work.component.html',
  styleUrls: ['./edit-work.component.scss']
})
export class EditWorkComponent implements OnInit {

  constructor(private api:ApiService) { }

  works:any = []
  selected:any = null

  showTitleControls:boolean = false;
  showDescControls:boolean = false;
  new_title:string = ""
  new_desc:string = ""

  message:string | null = null;


  ngOnInit(): void {
    this.api.getWorks().subscribe(response =>{
      this.works = response
    })
  }

  select(id:string){
    this.selected = this.works.find( (work:any) => (work._id == id) )
  }

  toggleTitleControls(){
    this.showTitleControls = !this.showTitleControls
  }

  toggleDescControls(){
    this.showDescControls = !this.showDescControls
  }

  update(){
    this.api.updateWork(this.selected._id, this.selected.titulo, this.selected.descripcion).subscribe(response =>{
      let res:any = response
      if(res.status){
        this.message = "Work updated!"
        setTimeout(() => {
          this.message = null
        }, 3000);
      }
    })
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }
  
  //apiUrl:string = 'http://localhost:4001';
  apiUrl:string = "https://espacios-backend.herokuapp.com"

  getInfo(){
    return this.http.get(`${this.apiUrl}/artistinfo`)
  }

  login(user:string, password:string){
    return this.http.get(`${this.apiUrl}/login?user=${user}&password=${password}`)
  }

  changeProfileImage(image:any){
    return this.http.put(`${this.apiUrl}/changeprofileimage`,{image})
  }

  uploadWork(titulo:any, image:any, desc:any){
    return this.http.post(`${this.apiUrl}/uploadwork`,{titulo,image,desc})
  }

  getWorks(){
    return this.http.get(`${this.apiUrl}/allworks`)
  }

  updateLinks(works:any){
    return this.http.put(`${this.apiUrl}/updateindex`,works)
  }

  updateWork(_id:string, title:string, desc: string){
    return this.http.put(`${this.apiUrl}/updatework`,{_id, title, desc})
  }

  updateInfo(name:string, desc:string){
    return this.http.put(`${this.apiUrl}/updateinfo`,{name,desc})
  }

}

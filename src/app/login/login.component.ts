import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private api:ApiService, private router: Router) { }

  user = ""
  pass = ""
  message = ""
  showMessage = false

  ngOnInit(): void {
  }

  handleClick(){
    this.api.login(this.user,this.pass).subscribe(response =>{
      let res:any = response
      console.log(res)
      if(res.status==0){
        this.message = "El usuario no se encuentra registrado"
        this.showMessage = true
      }else if(res.status==1){
        this.message = "Contraseña incorrecta"
        this.showMessage = true
      }else{
        window.localStorage.setItem("jwt",res.token)
        this.router.navigate(['/dashboard'])
      }
    })
  }

}
